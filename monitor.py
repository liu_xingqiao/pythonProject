import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from email.utils import formataddr
import os
import subprocess

# 配置发送邮件的账户信息
smtp_server = 'smtp.163.com'  # 邮件服务器地址
sender_email = 'moniBot@163.com'  # 发件人邮箱地址
sender_password = 'ZOLBAVXLAWRQWEEC'  # 发件人邮箱密码

# 配置接收邮件的账户信息
receiver_email = '610564337@qq.com'  # 收件人邮箱地址

# 获取设备日志
desktop_path = os.path.join(os.path.expanduser("~"), "Desktop")
logcat_path = desktop_path + '/文档/蜀影/抓包日志/手机_小米mix4抖音投屏音量调节无效.log'  # logcat文件路径，需要在AndroidManifest.xml中添加权限
# subprocess.call(['adb', 'pull', logcat_path])  # 从设备上拉取logcat文件

# 构建邮件内容
subject = "测试邮件"  # 邮件主题
body = "这是一封测试邮件"  # 邮件正文内容
msg = MIMEMultipart()  #
msg['From'] = formataddr(('Sender', sender_email))  # 设置发件人
msg['To'] = formataddr(('Recipient', receiver_email))  # 设置收件人
msg['Subject'] = subject  # 设置邮件主题

# 添加文本部分
text = MIMEText('This is a test email with an attachment.')
msg.attach(text)

# 添加附件
with open(logcat_path, "rb") as f:
    attachment = MIMEBase("application", "octet-stream")
    attachment.set_payload(f.read())
encoders.encode_base64(attachment)
msg.attach(attachment)

# 发送邮件
try:
    server = smtplib.SMTP()  # 创建SMTP对象
    server.connect(smtp_server)  # 连接SMTP服务器，端口号为587
    server.login(sender_email, sender_password)  # 登录SMTP服务器，需要提供账号和密码
    server.sendmail(sender_email, receiver_email, msg.as_string())  # 发送邮件
    server.quit()  # 关闭SMTP连接
    print("邮件发送成功")
except Exception as e:
    print("邮件发送失败:", e)