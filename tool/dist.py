import os
import sys
import PyInstaller.__main__

# 指定要打包的Python文件
script = 'dailyReport.py'

# 指定生成的可执行文件名
executable_name = 'your_executable_name'

# 使用PyInstaller打包Python文件
PyInstaller.__main__.run([
    '--onefile',  # 打包成一个独立的文件
    '--name', executable_name,  # 指定生成的可执行文件名
    script,  # 要打包的Python文件
])
