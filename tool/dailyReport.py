import datetime
import csv
import os
import pandas as pd

# # 获取当前日期和时间
# current_date = datetime.datetime.now().strftime('%Y-%m-%d')
# current_time = datetime.datetime.now().strftime('%H:%M:%S')
#
# # 获取用户输入的工作内容
# # while True:
# work_content = input('请输入工作内容：')
#
# # 定义要记录的数据
# logs = [
#     {'task': 'Task 1', 'duration': 2.5},
#     {'task': 'Task 2', 'duration': 3.0},
#     {'task': 'Task 3', 'duration': 1.5}
# ]
#
# # 获取桌面路径
# desktop_path = os.path.join(os.path.expanduser("~"), "Desktop")
#
# # 打开CSV文件，如果不存在则创建
# file_path = os.path.join(desktop_path, "example.xlsx")
# with open(file_path, 'w', newline='') as file:
#     fieldnames = ['task', 'duration']
#     writer = csv.DictWriter(file, fieldnames=fieldnames)
#
#     # 如果CSV文件为空，则写入表头
#     if file.tell() == 0:
#         writer.writeheader()
#
#     # 写入数据
#     for log in logs:
#         writer.writerow(log)
#
#     print('工作日报记录成功！')

# pip install -i https://pypi.douban.com/simple/ openpyxl

while True:
        current_date = datetime.datetime.now().strftime('%Y-%m-%d')
        date_array = []
        work_content_array = []

        work_content = input('请输入工作内容：')

        date_array.append(current_date)
        work_content_array.append(work_content)

        # 创建一个数据帧
        data = {'时间': date_array,
                '工作内容': work_content_array}
        # df = pd.DataFrame(data)
        df = pd.DataFrame()
        # df = df.append(data, ignore_index=True)
        df = df.append({'数据': work_content}, ignore_index=True)

        # 将数据帧写入Excel文件
        df.to_excel('output.xlsx', index=False)
